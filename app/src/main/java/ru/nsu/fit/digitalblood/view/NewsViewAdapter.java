package ru.nsu.fit.digitalblood.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.R;

public class NewsViewAdapter extends RecyclerView.Adapter<NewsViewAdapter.NewsViewHolder> {
    private List<String> news;

    public NewsViewAdapter(List<String> news) {
        this.news = new ArrayList<>(news);
//        this.news = news;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public NewsViewAdapter.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news,
                viewGroup, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewAdapter.NewsViewHolder newsViewHolder, int i) {
        newsViewHolder.title.setText(news.get(i));
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public void replaceNews(List<String> newNews) {
        this.news = new ArrayList<>(newNews);
//        this.news = newNews;
        notifyDataSetChanged();
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView title;

        NewsViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card);
            title = itemView.findViewById(R.id.news_name);
        }
    }
}
