package ru.nsu.fit.digitalblood.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.PersonPresenter;
import ru.nsu.fit.digitalblood.view.PersonView;
import ru.nsu.fit.digitalblood.view.activity.StartActivity;

import static android.app.Activity.RESULT_OK;

public class UserInfoFragment extends Fragment implements PersonView {
    private final int PICK_IMAGE_REQUEST = 71;
    @BindView(R.id.user_name_edit)
    protected AppCompatImageView userNameEdit;
    @BindView(R.id.user_email_edit)
    protected AppCompatImageView userEmailEdit;
    @BindView(R.id.user_name)
    protected AppCompatTextView userNameText;
    @BindView(R.id.user_email)
    protected AppCompatTextView emailText;
    @BindView(R.id.user_status)
    protected AppCompatTextView userStatusText;
    @BindView(R.id.user_description)
    protected AppCompatTextView userDescriptionText;
    @BindView(R.id.user_story)
    protected AppCompatTextView userStoryText;
    @BindView(R.id.button_logout)
    protected Button buttonLogout;
    @BindView(R.id.user_image)
    protected AppCompatImageView userImage;
    private Unbinder unbinder;
    private PersonPresenter personPresenter;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private Uri filePath;

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        personPresenter.getPersonData();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onUserNameChanged(String name) {
        userNameText.setText(name);
    }

    @Override
    public void onEmailChanged(String email) {
        emailText.setText(email);
    }

    @Override
    public void onStatusChangedToDoctor() {
        userStatusText.setText(R.string.doctor);
    }

    @Override
    public void onStatusChangedToPatient() {
        userStatusText.setText(R.string.patient);
    }

    @Override
    public void onPersonReady(String name, String email, String status, String description,
                              String medicalHistory, String imagePath) {
        userNameText.setText(name);
        emailText.setText(email);
        userStatusText.setText(status);
        userDescriptionText.setText(description);
        userStoryText.setText(medicalHistory);

        getImageFromDB(imagePath);

    }

    private void getImageFromDB(String imagePath) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final File tmpFile = File.createTempFile("img", "jpeg");

            storageReference.child("images").child(imagePath).getFile(tmpFile).addOnSuccessListener(taskSnapshot -> {
                Bitmap image = BitmapFactory.decodeFile(tmpFile.getAbsolutePath());

                userImage.setImageBitmap(image);
                progressDialog.dismiss();
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void setPersonPresenter(PersonPresenter personPresenter) {
        this.personPresenter = personPresenter;
    }

    @OnClick(R.id.user_name_edit)
    void onUserEditNameClicked() {
        EditNameDialog editNameDialog = new EditNameDialog();
        editNameDialog.setPersonPresenter(personPresenter);
        editNameDialog.show(getFragmentManager(), "EDIT_NAME");
    }

    @OnClick(R.id.user_email_edit)
    void onUserEditEmailClicked() {
        EditEmailDialog editEmailDialog = new EditEmailDialog();
        editEmailDialog.setPersonPresenter(personPresenter);
        editEmailDialog.show(getFragmentManager(), "EDIT_EMAIL");
    }

    @OnClick(R.id.user_status)
    void onStatusClicked() {
        ChooseStatusDialog chooseStatusDialog = new ChooseStatusDialog();
        chooseStatusDialog.setPersonPresenter(personPresenter);
        chooseStatusDialog.show(getFragmentManager(), "CHOOSE_STATUS");
    }

    @OnClick(R.id.button_logout)
    void onlogoutClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.logout_message))
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    FirebaseAuth firebaseAuth;
                    firebaseAuth = FirebaseAuth.getInstance();
                    firebaseAuth.signOut();

                    Intent intent = new Intent(getActivity(), StartActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                });
        builder.create().show();
    }

    @OnClick(R.id.user_image)
    void onImageChoose() {
        chooseImage();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            uploadImage();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                userImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.getMessage();
            }
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void setUserImage(String uuid) {
        personPresenter.onUserImageSet(uuid);
    }

    private void uploadImage() {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            String uuid = UUID.randomUUID().toString();
            StorageReference ref = storageReference.child("images/" + uuid);

            setUserImage(uuid);

            ref.putFile(filePath)
                    .addOnSuccessListener(taskSnapshot -> {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    })
                    .addOnProgressListener(taskSnapshot -> {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Uploaded " + (int) progress + "%");
                    });
        }
    }

}
