package ru.nsu.fit.digitalblood.view.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.PersonPresenter;

public class ChooseStatusDialog extends DialogFragment {
    private PersonPresenter personPresenter;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choose_status)
                .setItems(R.array.statuses, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            personPresenter.onPatientStatusChosen();
                            break;
                        case 1:
                            personPresenter.onDoctorStatusChosen();
                            break;
                        default:
                            break;
                    }
                });
        return builder.create();
    }

    public void setPersonPresenter(PersonPresenter personPresenter) {
        this.personPresenter = personPresenter;
    }
}
