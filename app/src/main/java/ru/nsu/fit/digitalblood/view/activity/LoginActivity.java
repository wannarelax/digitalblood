package ru.nsu.fit.digitalblood.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.nsu.fit.digitalblood.R;

public class LoginActivity extends AppCompatActivity {
    private static final Logger logger = LoggerFactory.getLogger(LoginActivity.class);

    @BindView(R.id.login_email)
    protected EditText email;

    @BindView(R.id.login_password)
    protected EditText password;

    private FirebaseAuth firebaseAuth;

    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuthListener = auth -> {
            FirebaseUser user = auth.getCurrentUser();
            if (user != null) {
                logger.info("onAuthStateChanged:signed_in: {}", user.getUid());
            } else {
                logger.info("onAuthStateChanged:signed_out");
            }
            updateUI(user);
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    private void signIn(String email, String password) {
        logger.debug("signIn:" + email);
        if (!validateForm()) {
            return;
        }

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    System.out.println("signInWithEmail:onComplete:" + task.isSuccessful());

                    if (!task.isSuccessful()) {
                        logger.debug("signInWithEmail:failed" + task.getException());
                        Toast.makeText(LoginActivity.this, R.string.auth_failed,
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String testEmail = email.getText().toString();
        if (TextUtils.isEmpty(testEmail)) {
            email.setError("Required.");
            valid = false;
        } else {
            email.setError(null);
        }

        String testPassword = password.getText().toString();
        if (TextUtils.isEmpty(testPassword)) {
            password.setError("Required.");
            valid = false;
        } else {
            password.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
        }
    }

    @OnClick(R.id.login_button)
    void onLogin() {
        signIn(email.getText().toString(), password.getText().toString());
    }
}
