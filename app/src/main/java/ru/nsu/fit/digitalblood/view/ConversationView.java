package ru.nsu.fit.digitalblood.view;

import java.util.List;

import ru.nsu.fit.digitalblood.model.ChatData;

public interface ConversationView {
    void onConversationLoaded(List<ChatData> chatDataList);
}
