package ru.nsu.fit.digitalblood.model;

/**
 * Class that represents news title
 */
public class News {
    private String title;

    /**
     * Empty constructor for Firebase
     */
    public News() {
    }

    /**
     * Creates news from title
     * @param title news title*/
    public News(String title) {
        this.title = title;
    }

    /**
     *Return news title
     * @return news title*/
    public String getTitle() {
        return title;
    }
}
