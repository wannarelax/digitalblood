package ru.nsu.fit.digitalblood.model.database;

import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ru.nsu.fit.digitalblood.model.ChatData;

public class MessageSender {
    public void sendMessage(String key, List<ChatData> messages) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child("CONVERSATION")
                .child(key)
                .setValue(messages);
    }
}
