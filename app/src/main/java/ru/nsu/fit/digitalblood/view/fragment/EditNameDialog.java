package ru.nsu.fit.digitalblood.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.PersonPresenter;

public class EditNameDialog extends DialogFragment {
    @BindView(R.id.name_input)
    protected EditText nameInput;

    private Unbinder unbinder;
    private PersonPresenter personPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_edit_name, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void setPersonPresenter(PersonPresenter personPresenter) {
        this.personPresenter = personPresenter;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_name_submit)
    void onUserNameSubmit() {
        String userName = nameInput.getText().toString();
        personPresenter.onUserNameSet(userName);
        dismiss();
    }
}
