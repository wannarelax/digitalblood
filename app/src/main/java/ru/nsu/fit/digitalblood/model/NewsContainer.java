package ru.nsu.fit.digitalblood.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for news
 **/
public class NewsContainer {
    private List<News> newsList;

    public NewsContainer() {
        this.newsList = new ArrayList<>();
    }

    public NewsContainer(List<News> newsList) {
        this.newsList = new ArrayList<>(newsList);
    }

    public void addNews(News news) {
        newsList.add(news);
    }

    public List<News> getNewsList() {
        return new ArrayList<>(newsList);
    }
}
