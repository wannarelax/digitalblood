package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.model.Chat;

public class ChatListLoader {
    /**
     * Loads chat list from DB
     *
     * @param chatListLoadCallback callback which will be called when the loading is finished
     */
    public void loadChatList(ChatListLoadCallback chatListLoadCallback) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        String uid = firebaseAuth.getCurrentUser().getUid();

        databaseReference.child("CHAT").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Chat>> genericTypeIndicator =
                        new GenericTypeIndicator<ArrayList<Chat>>() {
                        };
                List<Chat> chats = dataSnapshot.getValue(genericTypeIndicator);
                chatListLoadCallback.onChatListLoaded(chats);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @FunctionalInterface
    public interface ChatListLoadCallback {
        void onChatListLoaded(List<Chat> chats);
    }
}
