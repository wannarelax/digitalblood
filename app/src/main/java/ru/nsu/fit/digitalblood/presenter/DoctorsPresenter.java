package ru.nsu.fit.digitalblood.presenter;

import java.util.Map;

import ru.nsu.fit.digitalblood.model.Person;
import ru.nsu.fit.digitalblood.model.database.DoctorsLoader;
import ru.nsu.fit.digitalblood.view.DoctorsView;

/**
 * Presenter for doctors
 */
public class DoctorsPresenter {
    private DoctorsView doctorsView;

    private DoctorsCallback doctorsCallback;

    public DoctorsPresenter() {
        this.doctorsCallback = new DoctorsCallback();
    }

    /**
     * Attaches view to this presenter
     *
     * @param doctorsView View to be attached
     */
    public void attachView(DoctorsView doctorsView) {
        this.doctorsView = doctorsView;
    }

    /**
     * Starts loading doctors list*/
    public void onDoctorsRequired() {
        DoctorsLoader doctorsLoader = new DoctorsLoader();
        doctorsLoader.loadDoctors(doctorsCallback);
    }

    private class DoctorsCallback implements DoctorsLoader.DoctorsLoadCallback {
        @Override
        public void onDoctorsLoaded(Map<String, Person> doctors) {
            for (Map.Entry<String, Person> entry : doctors.entrySet()) {
                String key = entry.getKey();
                Person doctor = entry.getValue();
                doctorsView.onDoctorReady(doctor.getName(), doctor.getEmail(), doctor.getPhotoId(),
                        key);
            }
        }
    }
}

