package ru.nsu.fit.digitalblood.view;

public interface PersonView {
    void onUserNameChanged(String name);

    void onEmailChanged(String email);

    void onStatusChangedToDoctor();

    void onStatusChangedToPatient();

    void onPersonReady(String name, String email, String status, String decription,
                       String medicalHistory, String uuid);
}
