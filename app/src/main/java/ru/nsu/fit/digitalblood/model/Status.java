package ru.nsu.fit.digitalblood.model;

public enum Status {
    ADMIN,
    DOCTOR,
    PATIENT,
    UNDEFINED
}
