package ru.nsu.fit.digitalblood.presenter;

import ru.nsu.fit.digitalblood.model.database.PersonAdder;

public class SignUpPresenter {
    private PersonAdder personAdder;

    public SignUpPresenter() {
        this.personAdder = new PersonAdder();
    }

    public void onUserSignedUp() {
        personAdder.addPerson();
    }
}
