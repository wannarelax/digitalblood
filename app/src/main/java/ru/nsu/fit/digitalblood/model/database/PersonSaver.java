package ru.nsu.fit.digitalblood.model.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ru.nsu.fit.digitalblood.model.Person;

public class PersonSaver {
    /**
     * Saves person in database after registration
     *
     * @param person Person to save
     */
    public void savePerson(Person person) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("USER").child(user.getUid()).setValue(person);
    }
}
