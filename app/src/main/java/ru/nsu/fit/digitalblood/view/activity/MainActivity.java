package ru.nsu.fit.digitalblood.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.PersonPresenter;
import ru.nsu.fit.digitalblood.presenter.PresenterHolder;
import ru.nsu.fit.digitalblood.view.fragment.ChatFragment;
import ru.nsu.fit.digitalblood.view.fragment.DoctorsFragment;
import ru.nsu.fit.digitalblood.view.fragment.NewsFragment;
import ru.nsu.fit.digitalblood.view.fragment.UserInfoFragment;


public class MainActivity extends AppCompatActivity {
    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView bottomNavigationView;

    private PresenterHolder presenterHolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        attachPresenter();

        NewsFragment news = NewsFragment.getInstance();
        presenterHolder.getNewsPresenter().attachView(news);
        news.setNewsPresenter(presenterHolder.getNewsPresenter());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, news)
                .commit();

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_news:
                    NewsFragment newsFragment = NewsFragment.getInstance();
                    presenterHolder.getNewsPresenter().attachView(newsFragment);
                    newsFragment.setNewsPresenter(presenterHolder.getNewsPresenter());

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_container, newsFragment)
                            .commit();
                    break;
                case R.id.action_messages:
                    ChatFragment chatFragment = new ChatFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_container, chatFragment)
                            .commit();

                    break;
                case R.id.action_doctors:
                    DoctorsFragment doctorsFragment = DoctorsFragment.newInstance();
                    presenterHolder.getDoctorsPresenter().attachView(doctorsFragment);
                    doctorsFragment.setDoctorsPresenter(presenterHolder.getDoctorsPresenter());

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_container, doctorsFragment)
                            .commit();
                    break;
                case R.id.action_profile:
                    UserInfoFragment userInfoFragment = UserInfoFragment.newInstance();
                    presenterHolder.getPersonPresenter().attachView(userInfoFragment);
                    userInfoFragment.setPersonPresenter(presenterHolder.getPersonPresenter());

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_container, userInfoFragment)
                            .commit();
                    break;
            }
            return true;
        });
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenterHolder;
    }

    private void attachPresenter() {
        presenterHolder = (PresenterHolder) getLastNonConfigurationInstance();
        if (presenterHolder == null) {
            presenterHolder = new PresenterHolder();
        }
        PersonPresenter personPresenter = presenterHolder.getPersonPresenter();
    }
}
