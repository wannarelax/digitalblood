package ru.nsu.fit.digitalblood.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.model.ChatData;
import ru.nsu.fit.digitalblood.presenter.ConversationPresenter;
import ru.nsu.fit.digitalblood.view.ConversationAdapter;
import ru.nsu.fit.digitalblood.view.ConversationView;


public class ConversationActivity extends AppCompatActivity implements ConversationView {
    private RecyclerView mRecyclerView;
    private ConversationAdapter mAdapter;
    private EditText text;
    private Button send;

    private ConversationPresenter conversationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        conversationPresenter = new ConversationPresenter(this);

        Intent intent = getIntent();
        String otherUid = intent.getStringExtra(getString(R.string.uid));
        String otherName = intent.getStringExtra("NAME");
        String imageName = intent.getStringExtra("PIC");

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ConversationAdapter(setData());
        mRecyclerView.setAdapter(mAdapter);

        text = findViewById(R.id.et_message);

        send = findViewById(R.id.bt_send);
        send.setOnClickListener(view -> {
            if (!text.getText().toString().equals("")) {
                List<ChatData> messages = mAdapter.getItems();
                ChatData item = new ChatData();
                item.setTime("6:00pm");
                item.setType(FirebaseAuth.getInstance().getUid());
                item.setText(text.getText().toString());
                messages.add(item);
                conversationPresenter.onSendClicked(otherUid, messages);
                text.setText("");
            }
        });

        if (otherUid != null) {
            conversationPresenter.loadConversation(imageName, otherName, otherUid);
        }
    }

    @Override
    public void onConversationLoaded(List<ChatData> chatDataList) {
        mAdapter.setChatData(chatDataList);
    }

    public List<ChatData> setData() {
        return new ArrayList<>();
    }
}
