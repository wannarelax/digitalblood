package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import java.util.Objects;

public class ChatKey {
    private final String firstUid;
    private final String secondUid;

    public ChatKey(String firstUid, String secondUid) {
        this.firstUid = firstUid;
        this.secondUid = secondUid;
    }

    public String getFirstUid() {
        return firstUid;
    }

    public String getSecondUid() {
        return secondUid;
    }

    public String createKey() {
        int comparison = Objects.compare(firstUid, secondUid, String::compareTo);
        if (comparison > 0) {
            return firstUid + "|" + secondUid;
        } else {
            return secondUid + "|" + firstUid;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return firstUid + "|" + secondUid;
    }
}
