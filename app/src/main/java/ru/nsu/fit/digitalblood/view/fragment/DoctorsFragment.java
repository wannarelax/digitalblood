package ru.nsu.fit.digitalblood.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.DoctorsPresenter;
import ru.nsu.fit.digitalblood.view.DoctorInfo;
import ru.nsu.fit.digitalblood.view.DoctorsAdapter;
import ru.nsu.fit.digitalblood.view.DoctorsView;

public class DoctorsFragment extends Fragment implements DoctorsView {
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;

    private Unbinder unbinder;

    private DoctorsAdapter doctorsAdapter;

    private DoctorsPresenter doctorsPresenter;

    public static DoctorsFragment newInstance() {
        return new DoctorsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctors, container, false);
        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        DividerItemDecoration itemDecor = new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecor);

        doctorsAdapter = new DoctorsAdapter(doctorInfo -> { //No action required
        }, getActivity());
        recyclerView.setAdapter(doctorsAdapter);

        doctorsPresenter.onDoctorsRequired();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDoctorReady(String name, String email, String photoId, String key) {
        DoctorInfo doctorInfo = new DoctorInfo(name, email, photoId, key);
        doctorsAdapter.addDoctor(doctorInfo);
    }

    public void setDoctorsPresenter(DoctorsPresenter doctorsPresenter) {
        this.doctorsPresenter = doctorsPresenter;
    }
}

