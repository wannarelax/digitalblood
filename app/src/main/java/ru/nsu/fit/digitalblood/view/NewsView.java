package ru.nsu.fit.digitalblood.view;

import java.util.List;

public interface NewsView {
    void onNewsReady(List<String> titles);
}
