package ru.nsu.fit.digitalblood.presenter;

public class PresenterHolder {
    private PersonPresenter personPresenter;
    private NewsPresenter newsPresenter;
    private DoctorsPresenter doctorsPresenter;

    public PresenterHolder() {
        this.doctorsPresenter = new DoctorsPresenter();
        this.personPresenter = new PersonPresenter();
        this.newsPresenter = new NewsPresenter();
    }

    public PersonPresenter getPersonPresenter() {
        return personPresenter;
    }

    public NewsPresenter getNewsPresenter() {
        return newsPresenter;
    }

    public DoctorsPresenter getDoctorsPresenter() {
        return doctorsPresenter;
    }
}
