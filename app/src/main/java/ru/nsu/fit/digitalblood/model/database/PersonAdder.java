package ru.nsu.fit.digitalblood.model.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ru.nsu.fit.digitalblood.model.Person;
import ru.nsu.fit.digitalblood.model.Status;

public class PersonAdder {
    /**
     * Adds new person information to database
     **/
    public void addPerson() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        Person person = new Person();
        person.setEmail(user.getEmail());
        person.setName(user.getEmail());
        person.setStatus(Status.UNDEFINED);
        person.setPhotoId(null);
        person.setDescription("Nothing to show");
        person.setMedicalHistory("Nothing to show");

        person.setKey(user.getUid());

        databaseReference.child("USER").child(user.getUid()).setValue(person);
    }
}
