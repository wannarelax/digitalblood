package ru.nsu.fit.digitalblood.presenter;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import ru.nsu.fit.digitalblood.model.ChatData;
import ru.nsu.fit.digitalblood.model.database.ChatKey;
import ru.nsu.fit.digitalblood.model.database.ConversationLoader;
import ru.nsu.fit.digitalblood.model.database.MessageSender;
import ru.nsu.fit.digitalblood.view.ConversationView;

public class ConversationPresenter {
    private ConversationView conversationView;

    public ConversationPresenter(ConversationView conversationView) {
        this.conversationView = conversationView;
    }

    public void loadConversation(String imageName, String otherName, String otherUid) {
        String myUid = FirebaseAuth.getInstance().getUid();
        ChatKey chatKey = new ChatKey(myUid, otherUid);

        String conversationKey = chatKey.createKey();
        ConversationLoader conversationLoader = new ConversationLoader();
        conversationLoader.loadConversation(imageName, otherName, otherUid, conversationKey, chatDataList ->
                conversationView.onConversationLoaded(chatDataList));
    }

    public void onSendClicked(String otherUid, List<ChatData> messages) {
        String myUid = FirebaseAuth.getInstance().getUid();
        ChatKey chatKey = new ChatKey(myUid, otherUid);

        String conversationKey = chatKey.createKey();
        MessageSender messageSender = new MessageSender();
        messageSender.sendMessage(conversationKey, messages);
    }
}
