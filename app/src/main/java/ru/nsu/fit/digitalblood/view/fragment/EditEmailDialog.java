package ru.nsu.fit.digitalblood.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.PersonPresenter;

public class EditEmailDialog extends DialogFragment {
    @BindView(R.id.email_input)
    protected EditText emailInput;

    private Unbinder unbinder;
    private PersonPresenter personPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_edit_email, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setPersonPresenter(PersonPresenter personPresenter) {
        this.personPresenter = personPresenter;
    }

    @OnClick(R.id.edit_email_submit)
    void onUserEmailSubmit() {
        String userEmail = emailInput.getText().toString();
        personPresenter.onEmailSet(userEmail);
        dismiss();
    }
}
