package ru.nsu.fit.digitalblood.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.nsu.fit.digitalblood.R;

public class StartActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        update();
    }

    @OnClick(R.id.login)
    void onLoginClicked() {
        Intent intent = new Intent(StartActivity.this, LoginActivity.class);
        startActivityForResult(intent, 1);
        update();
    }

    @OnClick(R.id.create)
    void onCreateClicked() {
        Intent intent = new Intent(StartActivity.this, SignUpActivity.class);
        startActivityForResult(intent, 0);

        update();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 || requestCode == 1) {
            if (resultCode == 0) {
                update();
            }
        }
    }

    private void update() {
        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        updateUI(user);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent(StartActivity.this, MainActivity.class);
            startActivity(intent);

            finish();
        }
    }
}
