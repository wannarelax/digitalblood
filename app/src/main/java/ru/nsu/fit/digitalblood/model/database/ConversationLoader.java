package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.model.Chat;
import ru.nsu.fit.digitalblood.model.ChatData;

public class ConversationLoader {
    /**
     * Loads conversation from db
     *
     * @param key                      key of the conversation in db
     * @param conversationLoadCallback callback which will be called when the loading is finished
     */
    public void loadConversation(String picId, String otherName, String otherUid, String key, ConversationLoadCallback conversationLoadCallback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("CONVERSATION").child(key)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<ArrayList<ChatData>> genericTypeIndicator =
                                new GenericTypeIndicator<ArrayList<ChatData>>() {
                                };
                        List<ChatData> chatDataList = dataSnapshot.getValue(genericTypeIndicator);
                        if (chatDataList == null) {
                            Chat chat = new Chat();
                            chat.setName(otherName);
                            chat.setPersonUid(otherUid);
                            chat.setImage(picId);
                            changeList(chat);
                            chatDataList = new ArrayList<>();
                            databaseReference.child("CONVERSATION")
                                    .child(key)
                                    .setValue(chatDataList);
                        }
                        conversationLoadCallback.onConversationLoaded(chatDataList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void changeList(Chat newChat) {
        String uid = FirebaseAuth.getInstance().getUid();

        FirebaseDatabase.getInstance()
                .getReference()
                .child("CHAT")
                .child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<ArrayList<Chat>> genericTypeIndicator =
                                new GenericTypeIndicator<ArrayList<Chat>>() {
                                };
                        List<Chat> chats = dataSnapshot.getValue(genericTypeIndicator);
                        if (chats == null) {
                            chats = new ArrayList<>();
                        }
                        chats.add(newChat);
                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child("CHAT")
                                .child(uid)
                                .setValue(chats);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @FunctionalInterface
    public interface ConversationLoadCallback {
        void onConversationLoaded(List<ChatData> chatDataList);
    }
}
