package ru.nsu.fit.digitalblood.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.SignUpPresenter;

public class SignUpActivity extends AppCompatActivity {
    private static final Logger logger = LoggerFactory.getLogger(SignUpActivity.class);

    @BindView(R.id.signup_email)
    protected EditText email;

    @BindView(R.id.signup_password)
    protected EditText password;

    @BindView(R.id.signup_confirm_password)
    protected EditText confirmPassword;

    private FirebaseAuth firebaseAuth;

    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private SignUpPresenter signUpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuthListener = auth -> {
            FirebaseUser user = auth.getCurrentUser();
            if (user != null) {
                // User is signed in
                logger.info("onAuthStateChanged:signed_in: {}", user.getUid());
            } else {
                // User is signed out
                logger.info("onAuthStateChanged:signed_out");
            }
            updateUI(user);
        };

        signUpPresenter = new SignUpPresenter();
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    private void createAccount(String email, String password) {
        logger.debug("createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    logger.debug("createUserWithEmail:onComplete:" + task.isSuccessful());
                    if (!task.isSuccessful()) {
                        Toast.makeText(SignUpActivity.this,
                                R.string.auth_failed, Toast.LENGTH_SHORT).show();
                    } else {
                        signUpPresenter.onUserSignedUp();
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String testEmail = email.getText().toString();
        if (TextUtils.isEmpty(testEmail)) {
            email.setError("Required.");
            valid = false;
        } else {
            email.setError(null);
        }

        String testPassword = password.getText().toString();
        if (TextUtils.isEmpty(testPassword)) {
            password.setError("Required.");
            valid = false;
        } else {
            password.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
        }
    }

    @OnClick(R.id.signup_button)
    void onSignup() {
        if (password.getText().toString().equals(confirmPassword.getText().toString())) {
            createAccount(email.getText().toString(), password.getText().toString());
        } else {
            Toast.makeText(SignUpActivity.this, R.string.password_confirm_failed,
                    Toast.LENGTH_SHORT).show();
        }
    }

}
