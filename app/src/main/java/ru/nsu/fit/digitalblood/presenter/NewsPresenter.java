package ru.nsu.fit.digitalblood.presenter;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.model.News;
import ru.nsu.fit.digitalblood.model.NewsContainer;
import ru.nsu.fit.digitalblood.model.database.NewsLoader;
import ru.nsu.fit.digitalblood.view.NewsView;

public class NewsPresenter {
    private NewsView newsView;
    private NewsCallback newsCallback;

    public NewsPresenter() {
        this.newsCallback = new NewsCallback();
    }

    public void attachView(NewsView newsView) {
        this.newsView = newsView;
    }

    public void onNewsRequired() {
        NewsLoader newsLoader = new NewsLoader();
        newsLoader.loadNews(newsCallback);
    }

    private class NewsCallback implements NewsLoader.NewsLoadCallback {
        @Override
        public void onNewsLoaded(NewsContainer newsContainer) {
            List<String> titles = new ArrayList<>();
            for (News news : newsContainer.getNewsList()) {
                titles.add(news.getTitle());
            }

            newsView.onNewsReady(titles);
        }
    }
}
