package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.model.News;
import ru.nsu.fit.digitalblood.model.NewsContainer;

public class NewsLoader {
    public void loadNews(NewsLoadCallback newsLoadCallback) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        String uid = firebaseAuth.getCurrentUser().getUid();

        databaseReference.child("NEWS").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<News> newsList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String title = snapshot.getValue(String.class);
                    News news = new News(title);
                    newsList.add(news);
                }
                newsLoadCallback.onNewsLoaded(new NewsContainer(newsList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @FunctionalInterface
    public interface NewsLoadCallback {
        void onNewsLoaded(NewsContainer newsContainer);
    }
}
