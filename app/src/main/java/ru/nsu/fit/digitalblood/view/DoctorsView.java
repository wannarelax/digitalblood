package ru.nsu.fit.digitalblood.view;

public interface DoctorsView {
    void onDoctorReady(String name, String email, String photoId, String key);
}
