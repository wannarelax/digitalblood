package ru.nsu.fit.digitalblood.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.model.Chat;
import ru.nsu.fit.digitalblood.model.database.ChatListLoader;
import ru.nsu.fit.digitalblood.view.ChatAdapter;
import ru.nsu.fit.digitalblood.view.activity.ConversationActivity;

public class ChatFragment extends Fragment implements ChatAdapter.ChatViewHolder.ClickListener {
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.fab_add_dialog)
    protected FloatingActionButton addDialogButton;

    private ChatAdapter mAdapter;

    private ChatListCallback chatListCallback;

    public ChatFragment() {
        setHasOptionsMenu(true);
    }

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle a) {
        super.onCreate(a);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, null, false);
        unbinder = ButterKnife.bind(this, view);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ChatAdapter(setData(), this, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        chatListCallback = new ChatListCallback();
        ChatListLoader chatListLoader = new ChatListLoader();
        chatListLoader.loadChatList(chatListCallback);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public List<Chat> setData() {
        return new ArrayList<>();
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra(getString(R.string.uid), mAdapter.getPersonUid(position));
        intent.putExtra("NAME", mAdapter.getPersonName(position));
        intent.putExtra("PIC", mAdapter.getImage(position));
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClicked(int position) {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @OnClick(R.id.fab_add_dialog)
    void onAddDialogClicked() {
        ChooseDoctorDialog chooseDoctorDialog = new ChooseDoctorDialog();
        chooseDoctorDialog.show(getFragmentManager(), "CHOOSE_DOCTOR");
    }

    private class ChatListCallback implements ChatListLoader.ChatListLoadCallback {
        @Override
        public void onChatListLoaded(List<Chat> chats) {
            mAdapter.setChats(chats);
        }
    }
}
