package ru.nsu.fit.digitalblood.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.R;

public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.DoctorViewHolder> {
    private final OnClickListener listener;
    private List<DoctorInfo> doctors;

    private Context context;

    public DoctorsAdapter(OnClickListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.doctors = new ArrayList<>();
    }

    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item,
                viewGroup, false);
        return new DoctorViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder doctorViewHolder, int i) {
        doctorViewHolder.personAge.setText(doctors.get(i).getEmail());
        doctorViewHolder.personName.setText(doctors.get(i).getName());
        doctorViewHolder.bind(listener, doctors.get(i));

        getImageFromDB(doctors.get(i).getPhotoId(), doctorViewHolder.personPhoto);
    }

    private void getImageFromDB(String imagePath, ImageView personPhoto) {
        if (imagePath != null) {
            try {final ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Uploading...");
                progressDialog.show();

                FirebaseStorage storage;
                StorageReference storageReference;

                storage = FirebaseStorage.getInstance();
                storageReference = storage.getReference();

                final File tmpFile = File.createTempFile("img", "jpeg");

                storageReference.child("images").child(imagePath).getFile(tmpFile).addOnSuccessListener(taskSnapshot -> {
                    Bitmap image = BitmapFactory.decodeFile(tmpFile.getAbsolutePath());

                    personPhoto.setImageBitmap(image);
                    progressDialog.dismiss();
                });
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            personPhoto.setImageResource(R.drawable.ic_person_grey_24dp);
        }

    }

    @Override
    public int getItemCount() {
        return doctors.size();
    }

    public void addDoctor(DoctorInfo doctorInfo) {
        doctors.add(doctorInfo);
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onItemClicked(DoctorInfo doctorInfo);
    }

    public static class DoctorViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        private TextView personName;
        private TextView personAge;
        private ImageView personPhoto;

        public DoctorViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card);
            personName = itemView.findViewById(R.id.person_name);
            personAge = itemView.findViewById(R.id.person_age);
            personPhoto = itemView.findViewById(R.id.person_photo);
        }

        void bind(final OnClickListener listener, DoctorInfo doctorInfo) {
            cardView.setOnClickListener(v -> listener.onItemClicked(doctorInfo));
        }
    }
}