package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ru.nsu.fit.digitalblood.model.Person;

public class PersonLoader {
    /**
     * Loads person from database
     *
     * @param personLoadCallback callback which will be called when the loading is finished
     */
    public void loadPerson(PersonLoadCallback personLoadCallback) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        String uid = firebaseAuth.getCurrentUser().getUid();

        databaseReference.child("USER").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Person person = dataSnapshot.getValue(Person.class);
                personLoadCallback.onPersonLoaded(person);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @FunctionalInterface
    public interface PersonLoadCallback {
        void onPersonLoaded(Person person);
    }
}
