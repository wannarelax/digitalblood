package ru.nsu.fit.digitalblood.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.model.ChatData;

public class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int DATE = 0;
    private static final int YOU = 1;
    private static final int ME = 2;

    // The items to display in your RecyclerView
    private List<ChatData> items;

    private Context context;
    private String picPath;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ConversationAdapter(List<ChatData> items) {
        this.items = new ArrayList<>(items);
        this.context = context;
        this.picPath = picPath;
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        //More to come
        String myUid = FirebaseAuth.getInstance().getUid();
        if (items.get(position).getType().equals(myUid)) {
            return ME;
        } else {
            return YOU;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case DATE:
                View v1 = inflater.inflate(R.layout.layout_holder_date, viewGroup, false);
                viewHolder = new HolderDate(v1);
                break;
            case YOU:
                View v2 = inflater.inflate(R.layout.layout_holder_you, viewGroup, false);
                viewHolder = new HolderYou(v2);
                break;
            default:
                View v = inflater.inflate(R.layout.layout_holder_me, viewGroup, false);
                viewHolder = new HolderMe(v);
                break;
        }
        return viewHolder;
    }

    public void addItem(List<ChatData> item) {
        items.addAll(item);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case DATE:
                HolderDate vh1 = (HolderDate) viewHolder;
                configureViewHolder1(vh1, position);
                break;
            case YOU:
                HolderYou vh2 = (HolderYou) viewHolder;
                configureViewHolder2(vh2, position);
                break;
            default:
                HolderMe vh = (HolderMe) viewHolder;
                configureViewHolder3(vh, position);
                break;
        }
    }

    public void setChatData(List<ChatData> chatData) {
        this.items = chatData;
        notifyDataSetChanged();
    }

    private void configureViewHolder3(HolderMe vh1, int position) {
        vh1.getChatText().setText(items.get(position).getText());
    }

    private void configureViewHolder2(HolderYou vh1, int position) {
        vh1.getChatText().setText(items.get(position).getText());
    }

    private void configureViewHolder1(HolderDate vh1, int position) {
        vh1.getDate().setText(items.get(position).getText());
    }

    public List<ChatData> getItems() {
        return items;
    }
}
