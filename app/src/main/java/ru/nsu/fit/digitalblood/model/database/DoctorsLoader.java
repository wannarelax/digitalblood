package ru.nsu.fit.digitalblood.model.database;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import ru.nsu.fit.digitalblood.model.Person;
import ru.nsu.fit.digitalblood.model.Status;

public class DoctorsLoader {
    /**
     * Load doctors from database
     *
     * @param doctorsLoadCallback callback which will be called when the loading is finished
     */
    public void loadDoctors(DoctorsLoadCallback doctorsLoadCallback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("USER").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Person> doctors = new HashMap<>();

                GenericTypeIndicator<Map<String, Person>> genericTypeIndicator =
                        new GenericTypeIndicator<Map<String, Person>>() {
                        };
                Map<String, Person> personHashMap = dataSnapshot.getValue(genericTypeIndicator);

                for (Map.Entry<String, Person> entry : personHashMap.entrySet()) {
                    if (entry.getValue().getStatus() == Status.DOCTOR) {
                        doctors.put(entry.getKey(), entry.getValue());
                    }
                }

                doctorsLoadCallback.onDoctorsLoaded(doctors);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @FunctionalInterface
    public interface DoctorsLoadCallback {
        void onDoctorsLoaded(Map<String, Person> doctors);
    }
}

