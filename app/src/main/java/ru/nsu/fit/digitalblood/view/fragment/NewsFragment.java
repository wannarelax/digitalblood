package ru.nsu.fit.digitalblood.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.presenter.NewsPresenter;
import ru.nsu.fit.digitalblood.view.NewsView;
import ru.nsu.fit.digitalblood.view.NewsViewAdapter;

public class NewsFragment extends Fragment implements NewsView {
    @BindView(R.id.recycler_view)
    protected RecyclerView newsRecyclerView;

    private Unbinder unbinder;

    private NewsViewAdapter newsViewAdapter;

    private NewsPresenter newsPresenter;

    public static NewsFragment getInstance() {
        return new NewsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctors, container, false);
        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        newsRecyclerView.setLayoutManager(layoutManager);
        newsRecyclerView.setHasFixedSize(true);

        DividerItemDecoration itemDecor = new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL);
        newsRecyclerView.addItemDecoration(itemDecor);

        initializeAdapter();

        loadNews();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onNewsReady(List<String> titles) {
        newsViewAdapter.replaceNews(titles);
    }

    public void setNewsPresenter(NewsPresenter newsPresenter) {
        this.newsPresenter = newsPresenter;
    }

    private void loadNews() {
        newsPresenter.onNewsRequired();
    }

    private void initializeAdapter() {
        newsViewAdapter = new NewsViewAdapter(new ArrayList<>());
        newsRecyclerView.setAdapter(newsViewAdapter);
    }
}
