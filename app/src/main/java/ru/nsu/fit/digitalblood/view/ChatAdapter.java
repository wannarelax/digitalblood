package ru.nsu.fit.digitalblood.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.model.Chat;

public class ChatAdapter extends SelectableAdapter<ChatAdapter.ChatViewHolder> {
    private List<Chat> mArrayList;
    private ChatViewHolder.ClickListener clickListener;

    private Context context;

    public ChatAdapter(List<Chat> arrayList, ChatViewHolder.ClickListener clickListener, Context context) {
        this.mArrayList = new ArrayList<>(arrayList);
        this.clickListener = clickListener;

        this.context = context;
    }

    // Create new views
    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_chat, null);

        return new ChatViewHolder(itemLayoutView, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int position) {
        chatViewHolder.tvName.setText(mArrayList.get(position).getName());
        if (isSelected(position)) {
            chatViewHolder.checked.setChecked(true);
            chatViewHolder.checked.setVisibility(View.VISIBLE);
        } else {
            chatViewHolder.checked.setChecked(false);
            chatViewHolder.checked.setVisibility(View.GONE);
        }
        if (mArrayList.get(position).getImage() != null) {
            getImageFromDB(mArrayList.get(position).getImage(), chatViewHolder.userPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public void setChats(List<Chat> chats) {
        this.mArrayList = chats;
        notifyDataSetChanged();
    }

    public String getPersonUid(int position) {
        return mArrayList.get(position).getPersonUid();
    }

    public String getPersonName(int position) {
        return mArrayList.get(position).getName();
    }

    public String getImage(int position) {
        return mArrayList.get(position).getImage();
    }

    private void getImageFromDB(String imagePath, ImageView personPhoto) {
        if (imagePath != null) {
            try {
                final ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Uploading...");
                progressDialog.show();

                FirebaseStorage storage;
                StorageReference storageReference;

                storage = FirebaseStorage.getInstance();
                storageReference = storage.getReference();

                final File tmpFile = File.createTempFile("img", "jpeg");

                storageReference.child("images").child(imagePath).getFile(tmpFile).addOnSuccessListener(taskSnapshot -> {
                    Bitmap image = BitmapFactory.decodeFile(tmpFile.getAbsolutePath());

                    personPhoto.setImageBitmap(image);
                    progressDialog.dismiss();
                });
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            personPhoto.setImageResource(R.drawable.ic_person_grey_24dp);
        }

    }

    public static class ChatViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        private TextView tvName;
        private ImageView userPhoto;
        public CheckBox checked;
        private ClickListener listener;


        public ChatViewHolder(View itemLayoutView, ClickListener listener) {
            super(itemLayoutView);

            this.listener = listener;

            tvName = itemLayoutView.findViewById(R.id.tv_user_name);
            userPhoto = itemLayoutView.findViewById(R.id.iv_user_photo);
            checked = itemLayoutView.findViewById(R.id.chk_list);

            itemLayoutView.setOnClickListener(this);

            itemLayoutView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (listener != null) {
                return listener.onItemLongClicked(getAdapterPosition());
            }
            return false;
        }

        public interface ClickListener {
            void onItemClicked(int position);

            boolean onItemLongClicked(int position);

            boolean onCreateOptionsMenu(Menu menu);
        }
    }
}

