package ru.nsu.fit.digitalblood.model;

/**
 * Class that describes Person
 */
public class Person {
    private String email;
    private String name;
    private Status status;
    private String photoId;

    private String description;
    private String medicalHistory;

    private String key;

    /**
     * Empty constructor for Firebase
     */
    public Person() {
    }

    /**
     * Creates person from parameters
     *
     * @param status  User status
     * @param email   User email
     * @param name    User name (better first name and last name)
     * @param photoId id of image in profile
     */
    public Person(Status status, String email, String name, String photoId) {
        this.status = status;
        this.email = email;
        this.name = name;
        this.photoId = photoId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
