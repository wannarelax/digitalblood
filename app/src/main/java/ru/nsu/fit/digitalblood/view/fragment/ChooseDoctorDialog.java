package ru.nsu.fit.digitalblood.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.nsu.fit.digitalblood.R;
import ru.nsu.fit.digitalblood.model.Person;
import ru.nsu.fit.digitalblood.model.database.DoctorsLoader;
import ru.nsu.fit.digitalblood.view.DoctorInfo;
import ru.nsu.fit.digitalblood.view.DoctorsAdapter;
import ru.nsu.fit.digitalblood.view.activity.ConversationActivity;

public class ChooseDoctorDialog extends DialogFragment {
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;

    private DoctorsAdapter doctorsAdapter;

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctors, container, false);
        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        DividerItemDecoration itemDecor = new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecor);

        doctorsAdapter = new DoctorsAdapter(doctorInfo -> {
            Intent intent = new Intent(getActivity(), ConversationActivity.class);
            intent.putExtra(getString(R.string.uid), doctorInfo.getKey());
            intent.putExtra("NAME", doctorInfo.getName());
            intent.putExtra("PIC", doctorInfo.getPhotoId());
            startActivity(intent);
        }, getActivity());
        recyclerView.setAdapter(doctorsAdapter);

        DoctorsLoader doctorsLoader = new DoctorsLoader();
        doctorsLoader.loadDoctors(doctors -> {
            for (Map.Entry<String, Person> entry : doctors.entrySet()) {
                String key = entry.getKey();
                Person doctor = entry.getValue();
                DoctorInfo doctorInfo = new DoctorInfo(doctor.getName(), doctor.getEmail(),
                        doctor.getPhotoId(), key);
                doctorsAdapter.addDoctor(doctorInfo);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
