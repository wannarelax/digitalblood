package ru.nsu.fit.digitalblood.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.nsu.fit.digitalblood.R;


public class HolderMe extends RecyclerView.ViewHolder {
    private TextView chatText;

    public HolderMe(View v) {
        super(v);
        chatText = v.findViewById(R.id.tv_chat_text);
    }

    public TextView getChatText() {
        return chatText;
    }
}
