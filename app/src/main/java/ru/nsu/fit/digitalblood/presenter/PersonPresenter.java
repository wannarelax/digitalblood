package ru.nsu.fit.digitalblood.presenter;

import ru.nsu.fit.digitalblood.model.Person;
import ru.nsu.fit.digitalblood.model.Status;
import ru.nsu.fit.digitalblood.model.database.PersonLoader;
import ru.nsu.fit.digitalblood.model.database.PersonSaver;
import ru.nsu.fit.digitalblood.view.PersonView;

public class PersonPresenter {
    private PersonView personView;
    private Person person;

    private PersonSaver personSaver;

    private PersonCallback personCallback;

    public PersonPresenter() {
        this.personCallback = new PersonCallback();
        this.personSaver = new PersonSaver();
        //this.person = new Person(Status.UNDEFINED, "example@example.com", "Ivan Ivanov", 0);
    }

    public void attachView(PersonView personView) {
        this.personView = personView;
    }

    public void onUserNameSet(String userName) {
        person.setName(userName);
        personSaver.savePerson(person);
        personView.onUserNameChanged(person.getName());
    }

    public void onEmailSet(String userEmail) {
        person.setEmail(userEmail);
        personSaver.savePerson(person);
        personView.onEmailChanged(person.getEmail());
    }

    public void onDoctorStatusChosen() {
        person.setStatus(Status.DOCTOR);
        personSaver.savePerson(person);
        personView.onStatusChangedToDoctor();
    }

    public void onPatientStatusChosen() {
        person.setStatus(Status.PATIENT);
        personSaver.savePerson(person);
        personView.onStatusChangedToPatient();
    }

    public void getPersonData() {
        PersonLoader personLoader = new PersonLoader();
        personLoader.loadPerson(personCallback);
    }

    public void onUserImageSet(String imagePath) {
        person.setPhotoId(imagePath);
        personSaver.savePerson(person);
    }

    private class PersonCallback implements PersonLoader.PersonLoadCallback {
        @Override
        public void onPersonLoaded(Person person) {
            PersonPresenter.this.person = new Person();
            PersonPresenter.this.person.setName(person.getName());
            PersonPresenter.this.person.setEmail(person.getEmail());
            PersonPresenter.this.person.setStatus(person.getStatus());
            PersonPresenter.this.person.setDescription(person.getDescription());
            PersonPresenter.this.person.setMedicalHistory(person.getMedicalHistory());
            PersonPresenter.this.person.setPhotoId(person.getPhotoId());

            personView.onPersonReady(person.getName(), person.getEmail(), person.getStatus().name(),
                    person.getDescription(), person.getMedicalHistory(), person.getPhotoId());
        }
    }
}
